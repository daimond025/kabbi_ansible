#
[esxi:children]
esxi.kabbi1
#
# Group of machines on esxi.kabbi1 with distribution of local ip's in 192.168.40.0/24
[esxi.kabbi1]
ssl2                    loc_ip=192.168.40.2
api-paygate1            loc_ip=192.168.40.10
api-client1             loc_ip=192.168.40.20
api-worker1             loc_ip=192.168.40.21
api-order1              loc_ip=192.168.40.22
api-service1            loc_ip=192.168.40.23
api-phone-call1         loc_ip=192.168.40.24
api-pbx1                loc_ip=192.168.40.25
api-currency1           loc_ip=192.168.40.26
site1                   loc_ip=192.168.40.40
opcache1                loc_ip=192.168.40.48
opdata1                 loc_ip=192.168.40.50
sql2                    loc_ip=192.168.40.51
sql1                    loc_ip=192.168.40.52
service-engine1         loc_ip=192.168.40.54
api-geo1                loc_ip=192.168.40.62
service-websocket1      loc_ip=192.168.40.63
service-push1           loc_ip=192.168.40.64
gearman1                loc_ip=192.168.40.80
gearman-workers1        loc_ip=192.168.40.85
mq1                     loc_ip=192.168.40.90
nosql1                  loc_ip=192.168.40.110
db-geo1                 loc_ip=192.168.40.120
consul1                 loc_ip=192.168.40.150
logger1                 loc_ip=192.168.40.190
grafana1                loc_ip=192.168.40.200
influxdb1               loc_ip=192.168.40.201
netdata1                loc_ip=192.168.40.202
osrm1                   loc_ip=192.168.40.211
esearch1                loc_ip=192.168.40.220
webproxy1               loc_ip=192.168.40.230
router1                 loc_ip=192.168.40.254
#
# For properly overrides parameters
[esxi.kabbi1:children]
esxi.kabbi1.extnet1
esxi.kabbi1.extnet2
esxi.kabbi1.extnet3
ssl
app
gearman
mq
nosql
consul
logger
opcache
opdata
db.sql
db.geo
db.influxdb
netdata
osrm
esearch
webproxy
#
# Group of machines who use hetzner'es external network 136.243.216.24/29
[esxi.kabbi1.extnet1]
#                       ext_ip=136.243.216.24
ssl2                    ext_ip=136.243.216.25
#webproxy1              ext_ip=136.243.216.29
#webproxy1              ext_ip=136.243.216.30
#webproxy1              ext_ip=136.243.216.31
#
# Group of machines who use hetzner'es external network 148.251.173.192/29
[esxi.kabbi1.extnet2]
router1                 ext_ip=148.251.173.198 ext_ip6=2a01:4f8:211:e2b::ff
#
# Group of machines who use hetzner'es external network 2a01:4f8:211:e2b/64
[esxi.kabbi1.extnet3]
ssl2                    loc_ip6=2a01:4f8:211:e2b::25
router1                 loc_ip6=2a01:4f8:211:e2b::1
#
[ssl:children]
ssl.kabbi
#
[ssl.kabbi:children]
ssl.web
#
[ssl.web:children]
ssl.kabbi.web
#
[ssl.kabbi.web]
ssl2
#
[app:children]
app.fpm
app.nodejs
#
[app.fpm:children]
app.fpm.api_paygate
app.fpm.api_client
app.fpm.api_worker
app.fpm.api_order
app.fpm.api_service
app.fpm.api_phone_call
app.fpm.api_currency
app.fpm.site
app.fpm.gearman_workers
#
[app.nodejs:children]
app.nodejs.api_pbx
app.nodejs.service_engine
app.nodejs.api_geo
app.nodejs.service_websocket
app.nodejs.service_push
#
[app.fpm.api_paygate]
api-paygate1            app_name=api_paygate
#
[app.fpm.api_client]
api-client1             app_name=api_client
#
[app.fpm.api_worker]
api-worker1             app_name=api_worker
#
[app.fpm.api_order]
api-order1              app_name=api_order
#
[app.fpm.api_service]
api-service1            app_name=api_service
#
[app.fpm.api_phone_call]
api-phone-call1         app_name=api_phone_call
#
[app.fpm.api_currency]
api-currency1           app_name=api_currency app_php_version=7.1
#
[app.nodejs.api_pbx]
api-pbx1                app_name=api_pbx
#
[app.fpm.site]
site1                   app_name=site
#
[app.nodejs.service_engine]
service-engine1         app_name=service_engine
#
[app.nodejs.api_geo]
api-geo1                app_name=api_geo
#
[app.nodejs.service_websocket]
service-websocket1      app_name=service_websocket
#
[app.nodejs.service_push]
service-push1           app_name=service_push
#
[gearman]
gearman1
#
[app.fpm.gearman_workers]
gearman-workers1        app_name=gearman_workers
#
[db.sql]
sql[1:2]
#
[mq]
mq1
#
[opcache]
opcache1
#
[opdata]
opdata1
#
[nosql]
nosql1
#
[db.geo]
db-geo1
#
[consul]
consul1
#
[logger]
logger1
#
[db.influxdb]
influxdb1
#
[grafana]
grafana1
#
[netdata:children]
netdata.kabbi
#
[netdata.kabbi]
netdata1
#
[osrm]
osrm1
#
[esearch]
esearch1
#
[webproxy:children]
webproxy.kabbi
#
[webproxy.kabbi]
webproxy1
#
[router]
router1
#
