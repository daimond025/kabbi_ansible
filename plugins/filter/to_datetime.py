# Ported from ansible-2.4
from datetime import datetime

def to_datetime(string, format="%Y-%m-%d %H:%M:%S.%f"):
    return datetime.strptime(string, format)

class FilterModule(object):
    def filters(self):
        return {
            # datetime
            'datetime': to_datetime,
        }
