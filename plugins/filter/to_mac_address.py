def to_mac_address(host_ip_string, router_ip_string):
    """Return the string with mac address

    For example:
    host_ip_string="192.168.10.10"
    router_ip_string="192.168.10.254"
    then this return "00:00:00:fe:0a:0a"
    """
    host_ip_list = host_ip_string.split(".")
    router_ip_list = router_ip_string.split(".")

    # Continue if only networks is equal, and host is not
    # For example: 192.168.10.10 and 192.168.10.253
    if (host_ip_list[0:3] == router_ip_list[0:3]):
           # and host_ip_list[3] != router_ip_list[3]):
        mac_address_list = (
            "0",
            "0",
            "0",
            router_ip_list[3],
            host_ip_list[2],
            host_ip_list[3]
        )
        return ":".join(
            "{0:02x}".format(int(mac_part_string))
                for mac_part_string in mac_address_list
        )
    else:
        return ""

class FilterModule(object):
    def filters(self):
        return {
            # mac_address
            'mac_address': to_mac_address,
        }
